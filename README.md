﻿This is my editor's pass over Dwarf Fortress. I want to make the game more pleasant to read and play, but I'm a minimalist at heart. I don't stray from the vanilla files unless it's worth it.

You'll need DF v47.04. If you have a saved world, delete it.

This isn't an exhaustive list of changes, but it tries to be.

## I've improved the writing wherever I can:
* Every creature has a new description. (Taffer & Toothspit)
* Generated creature descriptions are also improved. For example, minotaurs have hooves and penguin people have spotted fur. Relevant bugs have been fixed. (Taffer)
* Every humanoid has new baby and child names. (Taffer & Dirst) 
* Helpful tooltips explain many reactions. (Button, Meph, & Taffer)
* The dictionary is *much* bigger than it used to be. *This feature is optional*. (GoblinCookie & Amostubal)
* Many more changes than mentioned above. For example, you *smelt* metal, you don't *make* it. Combat logs are more legible. (Taffer, Button, Dirst, Deon, & Igfig)

## Your game might be more fun to play:
* You can use chitin to create bone armor and crafts. (Taffer)
* Your dwarves can brew oats into beer. (Button)
* Your dwarves can empty buckets of water. (Button)
* Furry creatures yield fur, not leather. (Taffer)
* Dwarves are described with woolly limbs for some unique flavor. *This feature is optional*. (Taffer)
* Carve bone arrows and bolts in adventure mode. (Deon)
* You can carve more obsidian weapons. (Sver)
* Wooden maces and hammers can be crafted for training. (Sver & Warlord255)
* Feather tree eggs are edible. (Button)
* Most creatures make sounds. (Warlord255)
* More creatures will steal food, items, and alcohol. Beware monkeys. (Warlord255)
* Animal people have skills and talents that suit them. (Warlord255 & Taffer)
* Animal people have mannerisms. (Warlord255)
* Cat people clean themselves. (Warlord255)
* Some animal people have grasping tails. (Warlord255)
* Humanoids age if they didn't already.  (Taffer & Anonymous)
* Apart from golems, all humanoids now have a sex and reproduce. (Taffer)
* More creatures are geldable. (Taffer)
* Bird people have winged arms rather than arms and back-wings. This is more consistent with fanart and bird people drawn elsewhere. (Taffer)
* Butchered megabeasts made of stone, metal, and wood yield that material. (Button & Igfig)
* Some of the grass in good-aligned biomes will heal grazers. (Warlord255)

## Combat is a little better:
* Bodies aren't quite as fragile. Most organic tissue is tougher, for example, and ribs protect more. Most tissues heal now. Relevant bugs are fixed. (Sver, Grimlocke, Taffer, & Button)
* On the other hand, internal organs and eyeballs bleed more. (Sver, Grimlocke, & Taffer)
* Facial features are better positioned and can't be targeted or severed. Eyes are above noses and noses are above mouths, for example. (Sver & Taffer)
* Shoulders and hips now separate limbs from the body by one extra step. I adjusted armor and clothing to compensate. Necks and tails are now protected by default. (Taffer & Grimlocke)
* UBSTEP caps at 3 to prevent body armor from protecting toes due to a bug. (Taffer)
* Weapons are better differentiated from each other, and you can half-sword longswords. Whips aren't overpowered. (Jazzpirat!)
* Bird people have a combined talon scratch and kick attack. (Button)
* Many creatures with horns now head butt or gore with them. (Button)
* Compound eyes have a wider field of view. (Button)
* Giant predators eat dwarves, and many ambush prey. (Button)
* More spiders can trap with their webs. This includes some spider people. (Button)
* Giant insects bite. (Button)
* Plump helmet and sponge people now have knees and elbows. (Taffer)

## Bugs have been squished:
* Animal training is less bug-free and more enjoyable. Giant creatures are always tameable, giant insects live long enough to tame, more creatures are mounts, and more. (Button, Igfig, & Taffer)
* Animal people and giant variations have questionable tokens removed.  For example, animal people with legs can always jump. They don't meander or lay eggs, which fixes several bugs. (Button)
* Animal people become adults at age 12 and live for 60-80 years total. (Button & LargeSnail)
* Animal people and giant animals that spawn in pools spawn in lakes instead. (Button)
* Mussels and oysters provide pearls, like they're supposed to. (Button)
* Anacondas and hungry heads are too small to eat dwarves. (Button)
* A few creatures had teeth but didn't bite with them. (Button)
* Snail people and cave fish people are legless. (Button)
* Animal people don't need to eat a specific food. (Button)
* Toads, lice, and frogs are carnivores. (Button)
* Many flying vermin hunters now dive hunt. (Button)
* More creatures eat vermin. (Warlord255)
* Some creatures shouldn't be considered benign. (Button)
* Guineafowl appear in savanna, shrubland, desert, and grassland. (Button)

## I even have some "performance" improvements:
* Most organic materials rot. (Button, & Taffer)
* Unnatural fire sputters out on its own. (Sver) 
* Useless fruit seeds don't exist. (Doom Onion, TheBeardyMan, & Wannabehero)
* Small creatures won't yield leather or usable chitin. Some large creatures don't either, such as blizzard men and grimelings. (Taffer)

## There's plenty more that you probably wouldn't have noticed:
* Color schemes with a blue-tinted green won't have blue vomit. (Taffer)
* Mermaids can breathe air now. (Warlord255)
* Several creatures had their products listed several times. (Button)
* Magma crabs have crab bodies. (Button)
* Shoes, boots, and sandals can't be layered. (Sver)

## Some notes on the comments:
*Every* change that Revised makes to the raws is commented. The original contents of the token are listed beside it.

```
[USE_MATERIAL_TEMPLATE:CHITIN:CHITIN_NOYIELD_TEMPLATE]{:CHITIN_TEMPLATE}
```

{+} simply means that the token has been added.

```
[SKILL_LEARN_RATE:GRASP_STRIKE:150]{+}
[SKILL_LEARN_RATE:BITE:150]{+}
```

Sometimes I've felt it necessary to explain things.

```
[APPLY_CREATURE_VARIATION:RENAME_LEATHER_TO:FUR]{+  Revision: the leather is still furry after tanning.}
[CHILD:12]{:1  Revision: 1 is too young.}
```

Sometimes things are just 'commented out'.

```
[MEANDERER][NATURAL]{BENIGN}
```

I haven't added '{+}' to every line in cases where it seems obvious an entire section has been added.

```
[BODY:PEARL]{+  Revision: added pearls to fix bug 4108.}
	[BP:PEARL:pearl:STP][CONTYPE:UPPERBODY][SMALL][INTERNAL][CATEGORY:PEARL]
		[DEFAULT_RELSIZE:20]
		[VERMIN_BUTCHER_ITEM]
```

There's a few edge cases where things are a little different, but this should get you 90% of the way there.